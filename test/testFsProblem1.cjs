const fsProblem1 = require("../fs-problem1.cjs");
const path = require("path")

const absolutePathOfRandomDirectory = path.join(__dirname, 'random_files');
const randomNumberOfFiles = 2;

fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles, (err) => {
    if(err){
        console.error('Error: ', err);
    } else {
        console.log('Directory and files creation, deletion completed successfully.')
    }
});