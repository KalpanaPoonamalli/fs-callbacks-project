const fs = require("fs");
const path = require("path");


function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles, callback){

  let numberOfFilesCreated = 0;
  let numberOfFilesDeleted = 0 ;

  fs.mkdir(absolutePathOfRandomDirectory, (err) => {

    if(err){
      callback(err);
    } else {

      console.log(`Directory ${absolutePathOfRandomDirectory} created successfully.`)
        for (let index=1; index<=randomNumberOfFiles; index++){

          function deleteFiles(){
            for (let index1 = 1; index1<=randomNumberOfFiles; index1++){

              fs.unlink(`file${index1}.txt`, function(err){
                if(err){
                  callback(err)
                } else {
                  console.log(`file${index1}.txt is deleted.`)
                  numberOfFilesDeleted++

                  if (numberOfFilesDeleted === randomNumberOfFiles){
                    console.log("All files are deleted Successfully")

                    fs.rmdir(absolutePathOfRandomDirectory, (err) => {
                      if (err) {
                        callback(err);
                      } else {
                        console.log(`Directory ${absolutePathOfRandomDirectory} deleted.`)
                      }
                   })
                 }
               }

            })
          }
        }

          fs.writeFile(`file${index}.txt`, `This is file${index} data` , function(err){

              if(err){
                  console.error("Error in Creating File: ", err);
              } else {

                  console.log(`file${index}.txt is Created`)
                  numberOfFilesCreated++
                  

                  if(numberOfFilesCreated === randomNumberOfFiles){
                    console.log("All files Create Successfully")
                    deleteFiles()
                  }
                }

          })
        }
     }
  })
}


module.exports = fsProblem1;