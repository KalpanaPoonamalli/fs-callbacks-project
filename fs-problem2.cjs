const fs = require("fs")


function fsProblem2(filePath){

    fs.readFile(filePath, "utf8", function(err, data){
        if(err){
            console.error("Error: ", err)
        } else {

            console.log(data)

            const upperCaseData = data.toUpperCase();

            const newFile = "newFile.txt";

            fs.writeFile(newFile, upperCaseData, function (err){

                if (err){
                    console.error("Error in WriteFile: ", err)
                } else {
                    console.log("Uppercase data saved in newFile")

                    const newFilePath = "filenames.txt";


                    fs.appendFile( newFilePath,  newFile + "\n", function(err){

                        if(err){
                            console.error("Error in AppendFile: ", err)
                        } else {
                            console.log("newFile name saved in filenames.txt")

                            
                            fs.readFile(newFile, "utf8", function(err, data){
                                if(err){
                                    console.log("Error in reading newFile: ", err)
                                }
                                else {
                                    const lowerCaseData = data.toLowerCase()
                                    console.log(lowerCaseData)
                                    const splittingToSentances = lowerCaseData.split(/[.!?]/).filter(Boolean);
                                    console.log(splittingToSentances);

                                    const sortedSplittedSentences = splittingToSentances.sort();
                                    const sortedFile = "sortedFile.txt";


                                    fs.writeFile(sortedFile, sortedSplittedSentences.join("\n"), function (err){
                                        if(err){
                                            console.error("Error in writeFIle: ", err)
                                        } else {
                                            console.log("Sorted data saved in sortedFile")


                                            fs.appendFile(newFilePath, sortedFile + "\n", function(err){
                                                if(err){
                                                    console.error("Error in appending sortedFile: ", err);
                                                } else {
                                                    console.log("sortedFile name saved in filenames.txt");


                                                    fs.readFile(newFilePath, "utf8", function(err, filenames){
                                                        if(err){
                                                            console.error("Error in reading filenames: ", err);
                                                        } else {
                                                            const filenamesList = filenames.trim().split("\n")
                                                            console.log("filenames to delete")
                                                            console.log(filenamesList);

                                                            
                                                            filenamesList.map((filename) => {

                                                                fs.unlink(filename, function(err){
                                                                    if(err){
                                                                        console.error("Error in unlink: ", err)
                                                                    } else {
                                                                        console.log(`File ${filename} deleted.`)
                                                                    }

                                                                    
                                                                });
                                                                
                                                            });
                                                            
                                                        }
                                                    });
                                                    
                                                }
                                            });
                                            
                                        }
                                    });
                                    
                                }

                            });
                        }
                    });
                }
            });
        }
    });
}


module.exports = fsProblem2;